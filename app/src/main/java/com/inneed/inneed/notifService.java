package com.inneed.inneed;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.icu.util.DateInterval;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.JobIntentService;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.jar.Attributes;

/**
 * Created by marco on 19.02.2018.
 */

public class notifService extends JobIntentService {
    private static final String TAG = "Maps";

    private DatabaseReference mDatabase;

    private FirebaseAuth firebaseAuth ;
    private FirebaseUser firebaseUser ;
    // The entry points to the Places API.
    private GeoDataClient mGeoDataClient;
    private PlaceDetectionClient mPlaceDetectionClient;

    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;

    // A default location (Sydney, Australia) and default zoom to use when location permission is
    // not granted.
    private final LatLng mDefaultLocation = new LatLng(-33.8523341, 151.2106085);
    private static final int DEFAULT_ZOOM = 20;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;
    private LocationCallback mLocationCallback;

    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";

    static final int JOB_ID = 1000;
    private boolean removeLocation = false;

    private String state;
    static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, notifService.class, JOB_ID, work);

    }

    @Override
    protected void onHandleWork(final Intent intent) {

        state = intent.getStringExtra("test");
        if(state.equals("helper")){
            Log.e("helper","1");
            mDatabase  = FirebaseDatabase.getInstance().getReference("HelperPosition");


        }else{
            Log.e("needer","2");
            mDatabase  = FirebaseDatabase.getInstance().getReference("NeederPosition");
        }






        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {



                HashMap<String ,HashMap<String,Double>> hs = new HashMap<>(2);
                HashMap<String,Double> latlng = new HashMap<>();
                Log.e("test","lol3");
                if ( locationResult.getLocations().size() > 1){
                    latlng.put("lat",locationResult.getLastLocation().getLatitude());
                    latlng.put("lng",locationResult.getLastLocation().getLongitude());
                    hs.put(firebaseUser.getUid(),latlng);
                }else{
                    for (Location location : locationResult.getLocations()) {
                        // Update UI with location data
                        // ...



                        latlng.put("lat",location.getLatitude());
                        latlng.put("lng",location.getLongitude());
                        hs.put(firebaseUser.getUid(),latlng);




                    }
                }


                mDatabase.updateChildren((Map) hs);
            };
        };

        // Retrieve the content view that renders the map.


        // Construct a GeoDataClient.
        mGeoDataClient = Places.getGeoDataClient(this, null);

        // Construct a PlaceDetectionClient.
        mPlaceDetectionClient = Places.getPlaceDetectionClient(this, null);

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);


        getLocationPermission();

        getDeviceLocation();



    }

    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */

        try {

            if (mLocationPermissionGranted) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();

                locationResult.addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {

                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();

                            if(mLastKnownLocation != null) {


                                LocationRequest locationRequest = new LocationRequest();
                                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                                locationRequest.setInterval(5000);

                                if(removeLocation){

                                    mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
                                    stopSelf();

                                }
                                mFusedLocationProviderClient.requestLocationUpdates(locationRequest, mLocationCallback,
                                        null);


                            }else{

                                LocationRequest locationRequest = new LocationRequest();
                                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                                locationRequest.setInterval(1000);

                                mFusedLocationProviderClient.requestLocationUpdates(locationRequest, mLocationCallback,
                                        null);
                            }
                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());

                        }
                    }
                });
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }


    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            int off = 0;
            try {
                off = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            if(off==0){
                Intent onGPS = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(onGPS);
            }
            mLocationPermissionGranted = true;
        }else{
            mLocationPermissionGranted = false;


        }
    }

    /**
     * Handles the result of the request for location permissions.
     */






    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    @Override
    public int onStartCommand (Intent intent,
                               int flags,
                               int startId){
        onHandleWork(intent);
         return START_STICKY;
    }


    @Override
    public void onDestroy(){
        if (state.equals("needer")) {

            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("NeederPosition");
            mDatabase = mDatabase.child(firebaseUser.getUid());
            mDatabase.removeValue();
        }
        removeLocation = true;
    }
}

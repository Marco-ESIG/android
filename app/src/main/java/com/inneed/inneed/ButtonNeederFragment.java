package com.inneed.inneed;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ButtonNeederFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ButtonNeederFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ButtonNeederFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ImageButton chevronLeft;
    private ImageButton chevronRight;
    private Intent mServiceIntent;
    private RelativeLayout rlMain;

    private Button buttonUrgent;
    private String[] buttonText = {"Urgent", "Discret", "Aide"};
    private int[] buttonColor = {Color.BLUE, Color.GREEN, Color.RED};
    private int state = 0;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;

    public ButtonNeederFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ButtonNeederFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ButtonNeederFragment newInstance(String param1, String param2) {
        ButtonNeederFragment fragment = new ButtonNeederFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();


        MainActivity.mServiceIntent = new Intent(getActivity(), notifService.class);
        MainActivity.mServiceIntent.putExtra("test","helper");
        getActivity().startService(MainActivity.mServiceIntent);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_button_needer, container, false);
        chevronLeft =  view.findViewById(R.id.chevronLeft);
        chevronRight = view.findViewById(R.id.chevronRight);
        chevronLeft.setAlpha(0f);
        Button bGM = view.findViewById(R.id.button_maps);
        bGM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gmIntent = new Intent(getActivity(),MapsActivity.class);
                startActivity(gmIntent);


            }
        });

        rlMain =  view.findViewById(R.id.rlMain);
        rlMain.setOnTouchListener(new OnSwipeTouchListener(getActivity()){

            @Override
            public void onSwipeLeft() {

                if(state < buttonText.length-1){
                    state++;
                    buttonUrgent.setText(buttonText[state]);
                    buttonUrgent.setBackgroundColor(buttonColor[state]);

                }



                if(state > 0){
                    setLayoutVisible(chevronLeft);

                }


                if (state == buttonText.length-1){
                    setLayoutInvisible(chevronRight);
                }
            }

            @Override
            public void onSwipeRight() {

                if(state > 0){
                    state--;
                    buttonUrgent.setText(buttonText[state]);
                    buttonUrgent.setBackgroundColor(buttonColor[state]);
                }

                if(state == 0){
                    setLayoutInvisible(chevronLeft);
                }else if(state < buttonText.length){
                    setLayoutVisible(chevronRight);
                }
            }



        });

        buttonUrgent = view.findViewById(R.id.buttonUrgent);
        buttonUrgent.setText(buttonText[state]);
        buttonUrgent.setBackgroundColor(buttonColor[state]);
        buttonUrgent.setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        // Refresh the state of the +1 button each time the activity receives focus.

    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    public void setLayoutInvisible(final View v) {

        v.animate().alpha(0f)
                .setDuration(1000)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {

                    }
                });

    }




    public void setLayoutVisible(final View v) {

        v.animate().alpha(1f)
                .setDuration(1000)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {

                    }
                });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonUrgent:
                if(buttonUrgent.getText().equals(buttonText[0])){
                    MainActivity.mServiceIntent.putExtra("test","dis");
                    getActivity().stopService(MainActivity.mServiceIntent);

                    DatabaseReference mDatabase  = FirebaseDatabase.getInstance().getReference("HelperPosition");
                    mDatabase = mDatabase.child(firebaseUser.getUid());
                    mDatabase.removeValue();

                    FragmentManager manager = getFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    NeederFragment nd =  new NeederFragment();
                    transaction.hide(this);
                    transaction.replace(R.id.rlDash,nd);
                    transaction.addToBackStack(null);
                    transaction.commit();


                }
                break;
        }
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {

    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
